# coding=utf-8
__copyright__ = 'Copyright 2015, Abalt'

from oscar.apps.dashboard.catalogue import config


class CatalogueDashboardConfig(config.CatalogueDashboardConfig):
    name = 'eurotaxes.dashboard.catalogue'
